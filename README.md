# What is this?

This is a culmination of my digital design related work during the year 2017-2018.
It contains my work done during the semester, including my final assignment for the subject.

Feel free to reuse it, hack it, improve it in any way imaginable.

It is released under the MIT license.
