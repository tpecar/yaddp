# any intermediary error condition should finish the whole pipeline
set -o pipefail

# common functionality used by the scripts

# Execute command and print its invocation if it fails.
# Params: command [command parameters]*
ExecStep()
{
    "$@"
    RETVAL=$?
    if [ $RETVAL -ne 0 ]
    then
        echo "$0: Failed @ [$@]"
        exit $RETVAL
    fi
}

# Same as ExecStep, but runs the command in the specified working directory
# Params: [working directory] command [command parameters]*
ExecStepWd()
{
    (
        ExecStep cd "$1" &&
        ExecStep "${@:2}"
    )
    RETVAL=$?
    if [ $RETVAL -ne 0 ]
    then
        exit $RETVAL
    fi
}

# Print horizontal line, its length being the width of the terminal window
# Params: [char to use in line]
SepLine()
{
    COLS=$(tput cols)
    for (( i=0; i < $COLS ; i++ )); do printf "$1"; done
    echo
}

# constants used by scripts

export SCRIPT_ROOT=$(realpath $(dirname "$0"))

export SIM_DIR="./sim/"
export SYN_DIR="./syn/"

export AWK_LOG='
  /ERROR/ {print "\033[30;43m" $0 "\033[0m"; next}
  /WARN/ {print "\033[95m" $0 "\033[0m"; next}
  1
'
