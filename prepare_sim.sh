#!/bin/bash -f

# run all commands relative to script path
cd $(dirname $0)

source common.sh
source env_vivado.sh

PRJ_FILE="$1"
TOP_DESIGN_UNIT="$2"
if [ ! -n "$TOP_DESIGN_UNIT" ]
then
    echo "Usage: $0 PRJ_FILE TOP_DESIGN_UNIT"
    echo "TOP_DESIGN_UNIT is library.toplevel_entity"
    exit 1
fi

SNAPSHOT="$(basename $PRJ_FILE | cut -d"." -f1)_behav"

SepLine =

ExecStep rm -r "$SIM_DIR" &&
ExecStep mkdir "$SIM_DIR" &&

# project compilation
# Additional options: -93_mode
ExecStepWd "$SIM_DIR" xvhdl -v 2 --incr --relax -prj $(pwd)/$PRJ_FILE 2>&1 | tee -a "$SIM_DIR"\compile.log |
awk "$AWK_LOG" &&

# prevent Webtalk
ExecStep mkdir -p "$SIM_DIR"/xsim.dir/$SNAPSHOT/webtalk/ &&
ExecStep chmod u-rx "$SIM_DIR"/xsim.dir/$SNAPSHOT/webtalk/ &&

# run elaboration
# Additional options: -93_mode
ExecStepWd "$SIM_DIR" xelab -v 2 --incr --debug typical --relax --mt auto -L common -L secureip --snapshot $SNAPSHOT $TOP_DESIGN_UNIT -log elaborate.log |
awk "$AWK_LOG" &&

echo "Finished ok."
