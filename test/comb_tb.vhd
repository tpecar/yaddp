----------------------------------------------------------------------------------------------------
-- comb.vhd tests
----------------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.strlib.all;
use work.comb.all;

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

entity comb_tb is
begin
end entity;

----------------------------------------------------------------------------------------------------

architecture comb_tb_sim of comb_tb is
begin
  test: process is
    variable val : unsigned(3 downto 0) := "0000";
    variable enable : std_logic;

    variable mux_in : std_logic_vector(15 downto 0)     -- 16 bits in
      := "1111000010101100";
    variable mux_select : std_logic_vector(2 downto 0); -- 3 bits = 8 possible selections
    variable mux_out : std_logic_vector(1 downto 0);    -- 16 bits / 8 selects = 2 bits per select

    -- functions, procedures have to be testes directly (have to copy body here)
    -- because xsim does not allow stepping inside them

    -- procedure mux(
    variable vi_in : std_logic_vector(mux_in'range);
    variable vi_select : integer;
    variable vo_out : std_logic_vector(mux_out'range);
    -- )

    -- procedure demux(
    variable dvi_in : std_logic_vector(vo_out'range);
    variable dvi_select : integer;
    variable dvo_out : std_logic_vector(vi_in'range);
    -- ) is
  begin
    stdout := s'("Counter test for " * getattr * val) * endl;
    for i in 0 to 32 loop
    
      stdout := s'(s'("Val ") * ("" * to_integer(val) * rtrim * 5 ) *
                s'(" decoder ") * decoder(val) *
                s'(" encoder(decoder) ") * std_logic_vector(encoder(decoder(val))) ) * endl;
      counter(val, 15);
    end loop;

    stdout := "--------------------------------------------------------------------" * endl;
    
    val := to_unsigned(0, val'length);

    stdout := "Prescaler test" * endl;
    for i in 0 to 15 loop
      prescaler(val, enable, 2);
      stdout := "Val " * to_integer(val) * endl;
      if enable = '1' then
        stdout := "   enable" * endl;
      end if;
    end loop;

    stdout := "--------------------------------------------------------------------" * endl;

    stdout := ("Mux/Demux test" * endl) *
              ("  mux_in = " * std_logic_vector(mux_in)) * endl;
    for i in 0 to 10 loop
      --mux(mux_in, i, mux_out);

      --procedure mux(
      --  vi_in : in std_logic_vector;
      --  vi_select : in integer;
      --  vo_out : out std_logic_vector
      --) is

      vi_in := mux_in;
      --vo_out := (others => '0');
      vi_select := i;

      --begin
      -- remap vi_in to subvectors
      for v_cbit in 0 to vi_in'high loop
        if v_cbit / vo_out'length = vi_select then
          vo_out(v_cbit mod vo_out'length) := vi_in(v_cbit);
        end if;
      end loop;
      --end;

      mux_out := vo_out;

      stdout := "  Mux, select " * i * s'(" : ") * mux_out * endl;

      dvi_in := mux_out;
      dvi_select := i;

      -- procedure demux(
      --   vi_in : in std_logic_vector;
      --   vi_select : in integer;
      --   vo_out : out std_logic_vector
      -- ) is
      -- begin
        for v_cbit in 0 to dvo_out'high loop
          if v_cbit / dvi_in'length = dvi_select then
            dvo_out(v_cbit) := dvi_in(v_cbit mod dvi_in'length);
          end if;
        end loop;
      -- end;

      stdout := "  Demux, select " * i * s'(" : ") * dvo_out * endl;

      dvo_out := (others => '0');

    end loop;

    wait;
  end process;

end architecture;