----------------------------------------------------------------------------------------------------
-- string.vhd tests
----------------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.strlib.all;

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

entity strlib_tb is
begin
end entity;

----------------------------------------------------------------------------------------------------

architecture strlib_tb_sim of strlib_tb is
begin
  test: process
    variable testvar : std_logic_vector(4 downto 0) := "11001";
  begin
    -- stdout tests
    stdout := "stdout test " * endl;
    stdout := s'("stdout test ") * s'("with concatenation") * endl;
    stdout := "stdout test " * endl *
              s'("   in multiple") * endl *
              s'("   lines") * endl;
    stdout := "--------------------------------------------------------------------" * endl;
    -- string trim/align tests
    stdout := "trim/align test" * endl;
    stdout := s'("test" * ltrim * 30) * endl;
    stdout := s'("test" * rtrim * 30) * endl;

    stdout := "--------------------------------------------------------------------" * endl;

    -- std_logic_vector tests
    stdout := s'("vector test - bin input ") * std_logic_vector'("10101110") * endl *
              s'("vector test - hex input ") * std_logic_vector'(X"ABBA") * endl;
    stdout := "vector test multiple" * endl;
    for i in 0 to 256 loop
      -- "*" (string, T_Endl) sometimes failes to resolve - you have to explicitly qualify the
      -- string as string type (abbreviation is s)
      stdout := s'("   " * std_logic_vector(to_unsigned(i, 8)) * s'(" hex ") *
                hex * std_logic_vector(to_unsigned(i, 8))) * endl;
    end loop;

    stdout := "vector size test" * endl;

    -- Do note that even though the output is equivalent for the following statements
    --    stdout :=  T_GetHigh_Prefix'("test" * gethigh) * testvar * endl;
    --    stdout := "test" * ( gethigh * testvar ) * endl;
    -- but they invoke different functions. The first one requires the type qualifier for the
    -- compiler to properly resolve the function.
    --
    -- However, as stated above, the improper inference stems from the endl keyword - in cases like
    -- these, do a type qualification of the chain result to string
    --    stdout := s'( "test" * gethigh * testvar ) * endl;

    stdout := s'("test " * testvar * getattr * testvar) * endl;

    stdout := "--------------------------------------------------------------------" * endl;

    -- number conversion tests
    stdout := "integer test " * 12345 * endl;
    stdout := "integer test multiple" * endl;
    for i in 0 to 10 loop
      stdout := "   " * i * endl;
    end loop;

    stdout := "real test " * 1.2345 * endl;
    stdout := "real test multiple" * endl;
    for i in 0 to 10 loop
      stdout := "   " * (0.1 * i) * endl;
    end loop;
    stdout := "--------------------------------------------------------------------" * endl;

    -- combined
    stdout := s'(" combined test " * hex * std_logic_vector'("1110") * endl *
                s'(" with integers ") * 12345 * endl *
                s'(" and reals ") * 12.34
                ) * endl;
    wait;
  end process;
end architecture;