#!/bin/bash

# run all commands relative to script path
cd $(dirname $0)

source common.sh
source env_ise.sh

PRJ_FILE="$1"
if [ ! -n "$PRJ_FILE" ]
then
    echo "Usage: $0 PRJ_FILE"
    echo "Top design unit is assumed to be work.top"
    exit 1
fi

SepLine =
SepLine =

(echo "
set -tmpdir xst/projnav.tmp
set -xsthdpdir xst
run                                    \
-ifn $(pwd)/$PRJ_FILE                  \
-ifmt mixed                            \
-ofn TopModule                         \
-ofmt NGC                              \
-p xc3s500e-5-fg320                    \
-top top                               \
-opt_mode Speed                        \
-opt_level 1                           \
-iuc NO                                \
-keep_hierarchy No                     \
-netlist_hierarchy As_Optimized        \
-rtlview Yes                           \
-glob_opt AllClockNets                 \
-read_cores YES                        \
-write_timing_constraints NO           \
-cross_clock_analysis NO               \
-hierarchy_separator /                 \
-bus_delimiter <>                      \
-case Maintain                         \
-slice_utilization_ratio 100           \
-bram_utilization_ratio 100            \
-verilog2001 YES                       \
-fsm_extract YES -fsm_encoding Auto    \
-safe_implementation No                \
-fsm_style LUT                         \
-ram_extract Yes                       \
-ram_style Auto                        \
-rom_extract Yes                       \
-mux_style Auto                        \
-decoder_extract YES                   \
-priority_extract Yes                  \
-shreg_extract YES                     \
-shift_extract YES                     \
-xor_collapse YES                      \
-rom_style Auto                        \
-auto_bram_packing NO                  \
-mux_extract Yes                       \
-resource_sharing YES                  \
-async_to_sync NO                      \
-mult_style Auto                       \
-iobuf YES                             \
-max_fanout 100000                     \
-bufg 24                               \
-register_duplication YES              \
-register_balancing No                 \
-slice_packing YES                     \
-optimize_primitives NO                \
-use_clock_enable Yes                  \
-use_sync_set Yes                      \
-use_sync_reset Yes                    \
-iob Auto                              \
-equivalent_register_removal YES       \
-slice_utilization_ratio_maxmargin 5
"
) | ( ExecStepWd "$SYN_DIR" xst -intstyle ise -ofn ./TopModule.syr ) |
awk "$AWK_LOG" &&

ExecStepWd "$SYN_DIR" ngdbuild -intstyle ise -dd _ngo -nt timestamp -uc $(pwd)/Nexys2_koncna_seminarska.ucf -p xc3s500e-fg320-5 TopModule.ngc TopModule.ngd | awk "$AWK_LOG" &&
ExecStepWd "$SYN_DIR" map -intstyle ise -p xc3s500e-fg320-5 -cm area -ir off -pr off -c 100 -o TopModule_map.ncd TopModule.ngd TopModule.pcf | awk "$AWK_LOG" &&
ExecStepWd "$SYN_DIR" par -w -intstyle ise -ol high -t 1 TopModule_map.ncd TopModule.ncd TopModule.pcf | awk "$AWK_LOG" &&
ExecStepWd "$SYN_DIR" trce -intstyle ise -v 3 -s 5 -n 3 -fastpaths -xml TopModule.twx TopModule.ncd -o TopModule.twr TopModule.pcf | awk "$AWK_LOG" &&
ExecStepWd "$SYN_DIR" bitgen -intstyle ise -f $(pwd)/TopModule.ut TopModule.ncd | awk "$AWK_LOG" &&

echo y | ( ExecStepWd "$SYN_DIR" djtgcfg prog -d DOnbUsb -i 0 -f TopModule.bit ) | awk "$AWK_LOG" &&

echo "Finished ok."
