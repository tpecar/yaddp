#!/bin/bash -f

# run all commands relative to script path
cd $(dirname $0)

source common.sh
source env_vivado.sh

# exported, since we need this info in sim.tcl
export PRJ_FILE="$1"
export TOP_DESIGN_UNIT="$2"
export SNAPSHOT="$(basename $PRJ_FILE | cut -d"." -f1)_behav"

if [ ! -n "$TOP_DESIGN_UNIT" ]
then
    echo "Usage: $0 PRJ_FILE TOP_DESIGN_UNIT"
    echo "TOP_DESIGN_UNIT is library.toplevel_entity"
    exit 1
fi

# project run
ExecStep ./prepare_sim.sh "$PRJ_FILE" "$TOP_DESIGN_UNIT" &&

ExecStepWd "$SIM_DIR" xsim $SNAPSHOT -tclbatch $(pwd)/sim.tcl -log simulate.log -gui
