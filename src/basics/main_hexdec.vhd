library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.n2.all;
use work.rangelib.all;
use work.comb.all;
use work.numo.all;

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

architecture main_rtl of main is
  signal indata : std_logic_vector(3 downto 0);

begin

  indata <= n2_dsn_in.sw(3 downto 0);

  main_comb : process(indata) is
    variable decout : std_logic_vector(3 downto 0);
  begin
    binvec2bcd(unsigned(indata), decout);
    n2_dsn_out.Led(7 downto 4) <= indata;
    n2_dsn_out.Led(3 downto 0) <= decout;
  end process;
end architecture;