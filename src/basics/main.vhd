----------------------------------------------------------------------------------------------------
-- Simple test, for testing Nexys2 functionality.
----------------------------------------------------------------------------------------------------
-- The design is structured according to the recommendations present in the
-- "A structured VHDL design method" by Jiri Gaisler
----------------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.n2.all;
use work.rangelib.all;
use work.comb.all;
use work.numo.all;

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

architecture main_rtl of main is

  --------------------------------------------------------------------------------------------------
  -- design constants
  constant CLK_DIV : integer := CLK_FREQ / 2;

  --------------------------------------------------------------------------------------------------
  -- design synchronous state record type definition and signals
  type reg is record
    clk_prescaler_state : unsigned(to_high(CLK_DIV) downto 0);
    clk_prescaler_enable : std_logic;

    counter_state : unsigned(7 downto 0);
  end record;

  constant reg_reset : reg := (
    clk_prescaler_state   => (others => '0'),
    clk_prescaler_enable  => '0',
    counter_state         => (others => '0')
  );

  --------------------------------------------------------------------------------------------------
  -- signals that prepresent the registers
  --    r is the current state (driven by the main_seq process)
  --    rin is the next state (driven by the main_comb process)
  signal r, rin : reg;

  --------------------------------------------------------------------------------------------------
  -- aux design signals (component - logic binding)
  type counter2seg_T is record
    -- we can get the ranges based on instance attributes
    i_hex : unsigned(15 downto 0);
    o_ct : std_logic_vector(n2_out_default.seg'range);
    o_an : std_logic_vector(n2_out_default.an'range);
  end record;
  signal counter2seg : counter2seg_T;

  --------------------------------------------------------------------------------------------------
  -- reset signal
  signal rst : std_logic;

begin
  --------------------------------------------------------------------------------------------------
  -- reset bind
  rst <= n2_dsn_in.btn(0);

  --------------------------------------------------------------------------------------------------
  -- design component instantiation
  -- comp_name : entity work.component_entity(component_arch) generic map (...) port map (...);

  counter2seg_inst : entity work.hexvec2seg7(hexvec2seg7_rtl)
  generic map (
    cfg_refresh_Hz => 60
  )
  port map (
    i_hex => counter2seg.i_hex,
    o_ct => counter2seg.o_ct,
    o_an => counter2seg.o_an,

    clk => clk,
    rst => rst
  );

  -- non-registered
  n2_dsn_out.seg <= counter2seg.o_ct;
  n2_dsn_out.an <= counter2seg.o_an;

  --------------------------------------------------------------------------------------------------
  -- combinatoric process
  --
  -- Since this process is sensitive (or waits on change of) n2_dsn and r, it can basically
  -- contain both the asynchronous (non-registered) logic as the synchronous next-state calculation
  -- logic.
  --
  -- The general idea can be summed with the following rules
  --    n2_dsn_out <= [has n2_dsn_in as operand]  : asynchronous input/output
  --    n2_dsn_out <= [has v as an operand]       : synchronous output, changes pins
  --    v          <= [has n2_dsn_in as operand]  : synchronous input, changes state
  --    v          <= [has v as an operand]       : synchronous state machine logic, changes state
  --
  -- Do note that if you set n2_dsn_out depending on a state that was modified by n2_dsn_in
  -- dependant logic, it will be implemented as asynchronous logic.
  main_comb : process(rst, n2_dsn_in, r) is

    -- stores new register state during algorithm
    variable v : reg;
    
    variable counter_state_dec : std_logic_vector(15 downto 0);

  begin
    ------------------------------------------------------------------------------------------------
    -- default assignment of current register state
    v := r;

    ------------------------------------------------------------------------------------------------
    -- algorithm
    binvec2bcd(v.counter_state, counter_state_dec);
    counter2seg.i_hex <= unsigned(counter_state_dec);

    prescaler(v.clk_prescaler_state, v.clk_prescaler_enable, CLK_DIV);

    if v.clk_prescaler_enable = '1' then
      counter(v.counter_state, 2**8 - 1);
    end if;

    ------------------------------------------------------------------------------------------------
    -- reset condition

    -- we will use a button for reset signal
    if rst = '1' then
      v := reg_reset;
    end if;

    ------------------------------------------------------------------------------------------------
    -- drive module outputs
    n2_dsn_out.Led <= std_logic_vector(v.counter_state);

    ------------------------------------------------------------------------------------------------
    -- drive register inputs
    rin <= v;

  end process;

  --------------------------------------------------------------------------------------------------
  -- sequential process
  main_seq  : process(clk) is
  begin
    if rising_edge(clk) then
      r <= rin;
    end if;
  end process;

end architecture;