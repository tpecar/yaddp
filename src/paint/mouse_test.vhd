library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

architecture main_rtl of main is
	signal rst : std_logic;

	signal movX : signed(8 downto 0);
	signal movY : signed(8 downto 0);
	signal ps2data : std_logic_vector(23 downto 0);
	signal ps2ready : std_logic;
	signal dbg_state : unsigned(7 downto 0);

  signal segData : unsigned(7 downto 0);

begin

	seg7 : ENTITY WORK.Seg7(Behavioral) PORT MAP(clk, rst, n2_dsn_out.seg, n2_dsn_out.dp, n2_dsn_out.an, segData);
	ps2 : entity work.ps2(Behavioral) port map(rst, clk, n2_dsn_inout.PS2C, n2_dsn_inout.PS2D, movX, movY, open, open, open, ps2ready);

  segData <= unsigned(movX(7 downto 0));

	n2_dsn_out.Led(0) <= ps2ready;
	n2_dsn_out.Led(1) <= not n2_dsn_inout.PS2C;
	n2_dsn_out.Led(2) <= not n2_dsn_inout.PS2D;
  rst <= n2_dsn_in.btn(0);

	process(clk) is
	begin
		if clk'event and clk = '1' then
			if ps2ready = '1' then
			end if;
		end if;
	end process;

end architecture;