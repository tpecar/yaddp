----------------------------------------------------------------------------------
-- Developed by MP & TP.
-- 
-- Create Date:    21:35:47 08/09/2018 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Seg7 is
port(
	clk       : in std_logic;
	rst       : in std_logic;
	seg		 : out std_logic_vector (0 to 6);
	dp 		 : out std_logic;
	an 		 : out std_logic_vector (3 downto 0);
	data      : in unsigned (7 downto 0)
);
end Seg7;

architecture Behavioral of Seg7 is
	signal flag        : std_logic := '0';
	signal prescalerSeg: std_logic_vector (25 downto 0);
	
	type segTableType is array (0 to 15) of std_logic_vector(0 to 6); 
	constant segTable  : segTableType := (
	-- 0         1          2          3
		"1111110", "0110000", "1101101", "1111001",
	-- 4         5          6          7
		"0110011", "1011011", "1011111", "1110000",
	-- 8         9          A          B
		"1111111", "1111011", "1110111", "0011111",
	-- C         d          E          F
		"1001110", "0111101", "1001111", "1000111"
														);
begin
	process(clk) is 
	begin
		if clk'event and clk = '1' then
			prescalerSeg <= std_logic_vector(unsigned(prescalerSeg) + to_unsigned(1, prescalerSeg'length));
			
			if prescalerSeg = std_logic_vector(to_unsigned(250000, 26)) then
				if flag = '0' then
					flag <= '1';
				else
					flag <= '0';
				end if;
				prescalerSeg <= (others => '0');
			end if;			
					
			if rst = '1' then
				prescalerSeg <= (others => '0');
				flag <= '0';
			end if;
		end if;
	end process;

	process(flag, data) is
	begin
		an <= "1111";
		if flag = '0' then
			an <= "0111";
			seg <= not segTable(to_integer(data(7 downto 4)));
		else
			an <= "1011";
			seg <= not segTable(to_integer(data(3 downto 0)));
		end if;
	end process;
	
	dp <= '1';

end Behavioral;

