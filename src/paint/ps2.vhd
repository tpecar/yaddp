----------------------------------------------------------------------------------
-- Developed by TP.
-- 
-- Create Date:	11:38:04 08/13/2018 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

-- The mouse communication protocol is implemented according to
-- https://www.avrfreaks.net/sites/default/files/PS2%20Keyboard.pdf
-- http://www.isdaman.com/alsos/hardware/mouse/ps2interface.htm

entity ps2 is
	port (
		rst	 : in std_logic;
		clk	 : in std_logic;
		ps2c	: inout std_logic := 'Z';
		ps2d	: inout std_logic := 'Z';
		movX	: out signed(8 downto 0) := (others => '0');
		movY	: out signed(8 downto 0) := (others => '0');
		lclick : out std_logic;
		mclick : out std_logic;
		rclick : out std_logic;
		ready   : out std_logic := '0'
	);
end entity;

architecture Behavioral of ps2 is
	-- the current state of the bus
	-- the rx state machine has to ignore the bus during busTX and vice versa
	-- The idle mode is only after reset - by default the module should be in RX, and switch to TX only when
	-- new data is requested.
	type ps2StateBusType is (busIdle, busRX, busTX);
	signal ps2StateBus : ps2StateBusType := busIdle;

	-- host reads/writes bus on negative ps2c edge, device reads/writes bus on positive edge
	signal ps2cHost : std_logic := '0';

	-- rx byte & ready
	signal ps2rxData : unsigned(7 downto 0) := (others => '0');
	signal ps2rxComplete : std_logic := '0';

	-- tx byte & ready
	signal ps2txData : unsigned(7 downto 0) := (others => '0');
	signal ps2txComplete : std_logic := '0';

begin
	-- ps2 clock
	ps2clk : process(clk) is
		-- 500 kHz registration interval - (1/500 kHz) / (1/50 Mhz) = 100 divider, 7 bits
		variable prescaler : unsigned(6 downto 0) := (others => '0');
		variable ps2c0 : std_logic := '0'; -- last
		variable ps2c1 : std_logic := '0'; -- second last
	begin
		if clk'event and clk = '1' then
			-- single pulse only
			ps2cHost <= '0';

			if prescaler = to_unsigned(100, prescaler'length) then
				prescaler := (others => '0');

				-- negative edge
				if ps2c1 /= ps2c0 and ps2c0 = '0' then ps2cHost <= '1';
				end if;
				ps2c1 := ps2c0;
				ps2c0 := ps2c;
			else
				prescaler := prescaler + 1;
			end if;

			if rst = '1' then
				prescaler := (others => '0');
				ps2c0 := '0';
				ps2c1 := '0';
			end if;
		end if;
	end process;

	-- bus rx state machine
	ps2rx : process(clk) is
		-- RX states of the PS2 bus
		type ps2StateRX is (rx_start, rx_d0, rx_d1, rx_d2, rx_d3, rx_d4, rx_d5, rx_d6, rx_d7, rx_parity, rx_stop);
		type ps2StateRXArr is array (0 to ps2StateRX'pos(ps2StateRX'right)) of ps2StateRX;
		constant ps2StateRXc : ps2StateRXArr := (rx_start, rx_d0, rx_d1, rx_d2, rx_d3, rx_d4, rx_d5, rx_d6, rx_d7, rx_parity, rx_stop);

		variable state	: ps2StateRX := ps2StateRX'left;
		variable data	: unsigned(7 downto 0) := (others => '0');
	begin
		if clk'event and clk = '1' then

			if ps2StateBus = busRX and ps2rxComplete = '0' then
				-- clock & data driven by device
				ps2c <= 'Z';
				ps2d <= 'Z';

				-- read bits only on device provided clk
				-- since we have already detected clock edge, we can assume that data is also valid
				-- (no registration required)
				if ps2cHost = '1' then
					-- start is a noop

					-- rx data 0-7
					if  ps2StateRX'pos(state) > ps2StateRX'pos(rx_start) and
						ps2StateRX'pos(state) < ps2StateRX'pos(rx_parity)
					then
						data := shift_right(data, 1);
						data(7) := ps2d;
					end if;

					-- rx parity is a noop (no checks)
				
					if state = ps2StateRX'right then
						state := ps2StateRX'left;
						ps2rxComplete <= '1';
						ps2rxData <= data;
					else
						state := ps2StateRXc(ps2StateRX'pos(state) + 1);
					end if;
				end if;
			else
				-- single pulse only
				ps2rxComplete <= '0';
			end if;

			if rst = '1' then
				ps2rxComplete <= '0';
				ps2rxData <= (others => '0');

				state := ps2StateRX'left;
				data := (others => '0');
			end if;
		end if;
	end process;
	
	-- bus tx state machine
	ps2tx : process(clk) is
		-- TX states of the PS2 bus
		type ps2StateTX is (tx_inhibit, tx_d0, tx_d1, tx_d2, tx_d3, tx_d4, tx_d5, tx_d6, tx_d7, tx_parity, tx_stop, tx_ack);
		type ps2StateTXArr is array (0 to ps2StateTX'pos(ps2StateTX'right)) of ps2StateTX;
		constant ps2StateTXc : ps2StateTXArr := (tx_inhibit, tx_d0, tx_d1, tx_d2, tx_d3, tx_d4, tx_d5, tx_d6, tx_d7, tx_parity, tx_stop, tx_ack);

		variable state : ps2StateTX := ps2StateTX'left;
		variable data : unsigned(7 downto 0) := (others => '0');

		-- in the tx_inhibit state the master has to pull the clock for ~100us = 10 kHz
		-- (1/50 Mhz) / (1/10kHz) = 5000 divider, 13 bits
		variable inhibitPrescaler : unsigned(12 downto 0) := (others => '0');

		-- odd parity calculation for data bits
		-- reset to 1 for xor calculation
		variable parity : std_logic := '1';
	begin
		if clk'event and clk = '1' then

			if ps2StateBus = busTX and ps2txComplete = '0' then
				-- clock driven by device (unless in inhibit)
				ps2c <= 'Z';

				-- inhibit timed by prescaler
				if state = tx_inhibit then

					-- clock in inhibit, data not driven
					ps2c <= '0';
					ps2d <= 'Z';
					if inhibitPrescaler = to_unsigned(5000, inhibitPrescaler'length) then
						inhibitPrescaler := (others => '0');

						-- do a "Request to send" by releasing clock & pulling data low (start bit)
						ps2c <= 'Z';
						ps2d <= '0';
						state := tx_d0;

						-- register data byte
						data := ps2txData;
						
						-- reset parity calculation
						parity := '1';
					else
						inhibitPrescaler := inhibitPrescaler + 1;
					end if;
				else
					-- other states are driven by device clock
					if ps2cHost = '1' then
						-- start state is a noop (driven during request to send after inhibit)

						-- data 0-7 tx
						if  ps2StateTX'pos(state) > ps2StateTX'pos(tx_inhibit) and
							ps2StateTX'pos(state) < ps2StateTX'pos(tx_parity)
						then
							parity := parity xor data(0);
							ps2d <= data(0);
							data := shift_right(data, 1);
						end if;

						-- parity tx
						if state = tx_parity then
							ps2d <= parity;
						end if;

						-- stop (release data bus)
						if state = tx_stop then
							ps2d <= 'Z';
						end if;

						-- ack state is a noop (the device hopefully rx'ed correctly)
					
						if state = ps2StateTX'right then

							state := ps2StateTX'left;
							ps2txComplete <= '1';
						else
							state := ps2StateTXc(ps2StateTX'pos(state) + 1);
						end if;
					end if; -- if ps2cHost = '1' then
				end if; -- if state = tx_inhibit then
			else -- if ps2StateBus = busTX then
				-- single pulse only
				ps2txComplete <= '0';
			end if;

			if rst = '1' then
				ps2txComplete <= '0';
				state := ps2StateTX'left;
				inhibitPrescaler := (others => '0');
			end if;

		end if; -- if clk'event and clk = '1' then
	end process;

	-- ps2 mouse high-level machine
	ps2mouse : process(clk) is
		-- init
		type states is (tx_reset, rx_ack, rx_bat, rx_id, tx_edr, rx_edr_ack, rx_flags, rx_x, rx_y);
		variable state : states := states'left;

		variable flags : unsigned(7 downto 0) := (others => '0');
	begin
		if clk'event and clk = '1' then
			-- pulse only
			ready <= '0';

			case state is
				when tx_reset =>
					ps2txData <= x"FF";
					ps2StateBus <= busTX;

					if ps2txComplete = '1' then
						ps2StateBus <= busIdle;
						state := rx_ack;
					end if;

				-- rx's are noops, we don't check returned values
				when rx_ack =>
					ps2StateBus <= busRX;
					if ps2rxComplete = '1' then
						ps2StateBus <= busIdle;
						state := rx_bat;
					end if;

				when rx_bat =>
					ps2StateBus <= busRX;
					if ps2rxComplete = '1' then
						ps2StateBus <= busIdle;
						state := rx_id;
					end if;
				
				when rx_id =>
					ps2StateBus <= busRX;
					if ps2rxComplete = '1' then
						ps2StateBus <= busIdle;
						state := tx_edr;
					end if;
				
				when tx_edr =>
					ps2txData <= x"F4";
					ps2StateBus <= busTX;

					if ps2txComplete = '1' then
						ps2StateBus <= busIdle;
						state := rx_edr_ack;
					end if;

				when rx_edr_ack =>
					ps2StateBus <= busRX;
					if ps2rxComplete = '1' then
						ps2StateBus <= busIdle;
						state := rx_flags;
					end if;

				-- ps2 mouse STREAM mode data interpretation
				when rx_flags =>
					ps2StateBus <= busRX;
					if ps2rxComplete = '1' then
						ps2StateBus <= busIdle;
						flags := ps2rxData;
						lclick <= flags(0);
						rclick <= flags(1);
						mclick <= flags(2);
						state := rx_x;
					end if;

				when rx_x =>
					ps2StateBus <= busRX;
					if ps2rxComplete = '1' then
						ps2StateBus <= busIdle;
						movX <= signed(flags(4) & ps2rxData);
						state := rx_y;
					end if;

				when rx_y =>
					ps2StateBus <= busRX;
					if ps2rxComplete = '1' then
						ps2StateBus <= busIdle;
						movY <= signed(flags(5) & ps2rxData);
						ready <= '1';
						-- restart STREAM read cycle
						state := rx_flags;
					end if;

			end case;

			if rst = '1' then
				-- HiZ clock & data until TX
				ps2c <= 'Z';
				ps2d <= 'Z';
				-- reset state
				state := states'left;
				ps2StateBus <= busIdle;
				flags := (others => '0');
			end if;
		end if;
	end process;
end architecture;

