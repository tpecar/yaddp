----------------------------------------------------------------------------------
-- Developed by MP & TP.
-- 
-- Create Date:    19:39:12 07/25/2018 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity VGA is
port(
	clk       : in std_logic;
	rst       : in std_logic := '0';
	Hsync     : out std_logic;
	Vsync     : out std_logic;
	rowOut    : out unsigned (9 downto 0);
	colOut    : out unsigned (9 downto 0);
	vidon     : out std_logic;

	colEndDT  : out std_logic;
	rowEndDT  : out std_logic
);
end VGA;

architecture Behavioral of VGA is
	constant hst      : integer := 800;
	constant hsp      : integer := 96;
	constant hfp      : integer := 16;
	constant hbp      : integer := 48;
	constant hdt      : integer := 640;
	
	constant vst	  : integer := 525;
	constant vsp      : integer := 2;
	constant vfp      : integer := 10;
	constant vbp      : integer := 33;
	constant vdt      : integer := 480;

	signal prescaler : std_logic := '0';
begin


process(clk) is
	-- We can use variables to avoid the queueing nature of signals due to which a new signal value is available only in the next process cycle.
	--
	-- The general rule is that a signal can be written to multiple times within a process, but only the last assigned value is made available.
	-- Within a process, any read of signals value will use the signals value from the previous cycle.
	--
	-- In our case, defining row, col as signals caused the vidon comparison to stall for a cycle, which resulted in a missing 0,0 pixel.
	--
	-- More info on: https://www.nandland.com/vhdl/tips/variable-vs-signal.html
	--
	variable row       : unsigned (9 downto 0) := (others => '0');
	variable col       : unsigned (9 downto 0) := (others => '0');
begin
	if clk'event and clk = '1' then
		
		if prescaler = '1' then
			prescaler <= '0';
			
			if col < to_unsigned(hdt + hfp, col'length) or col >= to_unsigned(hdt + hfp + hsp, col'length)
				then Hsync <= '1'; else Hsync <= '0';
			end if;
			
			if row < to_unsigned(vdt + vfp, row'length) or row >= to_unsigned(vdt + vfp + vsp, row'length)
				then Vsync <= '1'; else Vsync <= '0';
			end if;
			
			col := col + to_unsigned(1, col'length);
			if col = to_unsigned(hst, col'length) then
				row := row + to_unsigned(1, row'length);
				col := (others => '0');
			end if;

			if row = to_unsigned(vst, row'length) then
				row := (others => '0');
			end if;

			if col >= to_unsigned(hdt, col'length) then
				colEndDT <= '1'; else colEndDT <= '0';
			end if;
			if row >= to_unsigned(vdt, col'length) then
				rowEndDT <= '1'; else rowEndDT <= '0';
			end if;
			if col < to_unsigned(hdt, col'length) and row < to_unsigned(vdt, row'length) then
				vidon <= '1'; else vidon <= '0';
			end if;
		else
			prescaler <= '1';
		end if;
		
		if rst = '1' then
			prescaler <= '0';

			row := (others => '0');
			col := (others => '0');

			Hsync <= '0';
			Vsync <= '0';
			vidon <= '0';
		end if;
		
		rowOut <= row;
		colOut <= col;
	end if;
end process;

end Behavioral;






