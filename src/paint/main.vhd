----------------------------------------------------------------------------------
-- Developed by TP.
-- 
-- Create Date:    21:28:44 08/09/2018 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use IEEE.MATH_REAL.ALL;

-- mux, demux
use work.comb.all;

architecture main_rtl of main is
	signal rst : std_logic;

	signal segData : unsigned(7 downto 0);

	signal movX : signed(8 downto 0);
	signal movY : signed(8 downto 0);
	signal lclick : std_logic;
	signal mclick : std_logic;
	signal rclick : std_logic;
	signal ps2ready : std_logic;

	signal row		: unsigned (9 downto 0);
	signal col		: unsigned (9 downto 0);
	signal colEndDT : std_logic;
	signal rowEndDT : std_logic;
	signal HsyncReg : std_logic; -- registered so that it will be synchronized to video
	signal VsyncReg : std_logic;

	signal vidon	: std_logic;

	-- 640 x 480 px drawing area, 4x4px = 1 game px
	-- 160 x 120 game px
	type PosType is record
		x : unsigned(7 downto 0); -- log2 160 game px
		y : unsigned(6 downto 0); -- log2 120 game px
	end record;

	signal pointer : PosType := (to_unsigned(50, 8), to_unsigned(50, 7));

	-- game coordinates to memory map
	-- row = 480/120 = 4, col = 640/160 = 4, 4 = 2**2, lower 2 bits of original px address are unused
	function getMemAddr(gcRow : unsigned(6 downto 0); gcCol : unsigned(7 downto 0))
	return unsigned is
		variable o : unsigned(12 downto 0) := (others => '0');
	begin
		-- 120 rows by 160 columns rounded to the next power of 2 to
		-- 128 rows by 256 columns, 4 2-bit groups packed into one addressable unit
		o := gcRow & gcCol(7 downto 2);
		return o;
	end function;

	signal ramReady : std_logic;

	signal addr0   	: unsigned(12 downto 0);
	signal we0		: std_logic := '0';
	signal datain0	: unsigned(7 downto 0);
	signal dataout0	: unsigned(7 downto 0);

	signal addr1   	: unsigned(12 downto 0);
	signal we1		: std_logic := '0';
	signal datain1	: unsigned(7 downto 0);
	signal dataout1	: unsigned(7 downto 0);

	-- packed RGB values
	type VGAMapType is record
		r : std_logic_vector(2 downto 0);
		g : std_logic_vector(2 downto 0);
		b : std_logic_vector(1 downto 0);
	end record;

	-- function for converting web RGB to Nexys2 VGA values
	function RGB(i : unsigned(23 downto 0))
	return VGAMapType is
		variable o : VGAMapType;
	begin
		o.r := std_logic_vector(to_unsigned(natural(round(7.0 * (real(to_integer(i(23 downto 16))) / 255.0))), o.r'length));
		o.g := std_logic_vector(to_unsigned(natural(round(7.0 * (real(to_integer(i(15 downto 8))) / 255.0))), o.g'length));
		o.b := std_logic_vector(to_unsigned(natural(round(3.0 * (real(to_integer(i(7 downto 0))) / 255.0))), o.b'length));
		return o;
	end function;

	-- color palette
	type PaletteArray is array (0 to 15) of VGAMapType;
	constant palette : PaletteArray := (
		RGB(x"000000"), RGB(x"FFFFFF"), RGB(x"FF0000"), RGB(x"00FF00"),
		RGB(x"0000FF"), RGB(x"FFFF00"), RGB(x"00FFFF"), RGB(x"FF00FF"),
		RGB(x"C0C0C0"), RGB(x"808080"), RGB(x"800000"), RGB(x"808000"),
		RGB(x"008000"), RGB(x"800080"), RGB(x"008080"), RGB(x"000080")
	);
	-- over which palette entry the mouse is currently hovering
	signal paletteHover : unsigned(3 downto 0) := (others => '0');

	-- current palette
	type activePaletteArray is array (0 to 3) of unsigned(3 downto 0);
	signal activePalette : activePaletteArray := (
		-- default palette
		x"0", x"1", x"2", x"3"
	);
	-- over which selected palette entry the mouse is currently hovering
	signal activePaletteHover : unsigned(1 downto 0) := (others => '0');

	-- menu positions
	constant MENU_Palette_X : integer := 135;
	constant MENU_Active_Palette_X : integer := 110;

begin
	rst <= n2_dsn_in.btn(0);

	-- components
	seg7 : ENTITY WORK.Seg7(Behavioral) PORT MAP(clk, rst, n2_dsn_out.seg, n2_dsn_out.dp, n2_dsn_out.an, segData);
	Vga  : ENTITY WORK.VGA(Behavioral) PORT MAP (clk, open, HsyncReg, VsyncReg, row, col, vidon, colEndDT, rowEndDT);
	ps2 : entity work.ps2(Behavioral) port map(rst, clk, n2_dsn_inout.PS2C, n2_dsn_inout.PS2D, movX, movY, lclick, mclick, rclick, ps2ready);
	--mouse : entity work.ps2_mouse(logic)
	--	port map(clk, not rst, n2_dsn_inout.PS2C, n2_dsn_inout.PS2D, ps2data, ps2ready);

	ram  : entity work.ram64k(Behavioral) port map(
		rst, clk, ramReady,
		addr0, we0, datain0, dataout0,
		addr1, we1, datain1, dataout1
	);
	
	-- debug
	n2_dsn_out.Led(0) <= not n2_dsn_inout.PS2C;
	n2_dsn_out.Led(1) <= not n2_dsn_inout.PS2D;

	-- video gen
	process(clk) is
		-- registered memory data
		variable memData : unsigned(7 downto 0) := (others => '0');
		variable curPixel : std_logic_vector(1 downto 0);

		variable r : std_logic_vector(3 downto 1);
		variable g : std_logic_vector(3 downto 1);
		variable b : std_logic_vector(2 downto 1);
	begin
		if clk'event and clk='1' then
			n2_dsn_out.vgaRed <= "000";
			n2_dsn_out.vgaGreen <= "000";
			n2_dsn_out.vgaBlue <= "00";
			
			if vidon = '1' and ramReady = '1' then
				-- load next column group (which will be current in the next cycle)
				if col(3 downto 0) = "0000" then
					memData := dataout0;
					addr0 <= getMemAddr(row(8 downto 2), col(9 downto 2)+4);
				end if;

				-- pixel display
				mux(std_logic_vector(memData), to_integer(col(3 downto 2)), curPixel);
				r := palette(
					to_integer(activePalette(to_integer(unsigned(curPixel))))
					).r;
				g := palette(
					to_integer(activePalette(to_integer(unsigned(curPixel))))
					).g;
				b := palette(
					to_integer(activePalette(to_integer(unsigned(curPixel))))
					).b;

				-- active palette display
				if rclick = '1' and col(9 downto 2) > MENU_Active_Palette_X and row(9 downto 4) < 4 then
					r := palette(
						to_integer(activePalette(to_integer(row(6 downto 4))))
						).r;
					g := palette(
						to_integer(activePalette(to_integer(row(6 downto 4))))
						).g;
					b := palette(
						to_integer(activePalette(to_integer(row(6 downto 4))))
						).b;
					if 
						row(9 downto 4) = activePaletteHover and
						row(1) = '1' and col(1) = '1'
					then
						r := not r; g := not g; b := not b;
					end if;
				end if;

				-- palette display
				if rclick = '1' and col(9 downto 2) > MENU_Palette_X and row(9 downto 4) < 16 then
					r := palette(to_integer(row(8 downto 4))).r;
					g := palette(to_integer(row(8 downto 4))).g;
					b := palette(to_integer(row(8 downto 4))).b;
					if 
						row(9 downto 4) = paletteHover and
						row(1) = '1' and col(1) = '1'
					then
						r := not r; g := not g; b := not b;
					end if;
				end if;

				-- pointer display
				if col(9 downto 2) = pointer.x and row(9 downto 2) = pointer.y and
					(
						col(1 downto 0) = "00" or col(1 downto 0) = "11" or
						row(1 downto 0) = "00" or row(1 downto 0) = "11"
					)
				then
					r := not r; g := not g; b := not b;
				end if;

				n2_dsn_out.vgaRed <= r;
				n2_dsn_out.vgaGreen <= g;
				n2_dsn_out.vgaBlue <= b;
			end if;

			-- preload next screen first byte
			if rowEndDT = '1' then
				addr0 <= (others => '0');
			end if;

			-- preload next line first byte
			if colEndDT = '1' and rowEndDT = '0' then
				if row(1 downto 0) = "11" then
					addr0 <= getMemAddr(row(8 downto 2)+1, "00000000");
				else
					addr0 <= getMemAddr(row(8 downto 2), "00000000");
				end if;
			end if;

			if rst = '1' then
				memData := (others => '0');
			end if;

			n2_dsn_out.Hsync <= HsyncReg;
			n2_dsn_out.Vsync <= VsyncReg;
		end if;
	end process;

	-- mouse
	process(clk) is
		variable newPointerX : signed(8 downto 0);
		variable newPointerY : signed(8 downto 0);
		variable newPointer : PosType;
		variable cdata : std_logic_vector(7 downto 0);

		type memStates is (memIdle, memAddr, memWait, memUpdate);
		variable state : memStates := memIdle;
	begin
		if clk'event and clk = '1' then
			if ps2ready = '1' then
				newPointerX := signed(std_logic_vector'("0") & std_logic_vector(pointer.x)) + movX;
				newPointerY := signed(std_logic_vector'("0") & std_logic_vector(pointer.y)) - movY;
				
				-- restrict to boundaries
				if newPointerX >= 160 then
					newPointerX := to_signed(159, 9);
				end if;
				if newPointerX < 0 then
					newPointerX := (others => '0');
				end if;

				if newPointerY >= 120 then
					newPointerY := to_signed(119, 9);
				end if;
				if newPointerY < 0 then
					newPointerY := (others => '0');
				end if;

				newPointer.x := unsigned(newPointerX(7 downto 0));
				newPointer.y := unsigned(newPointerY(6 downto 0));
				pointer <= newPointer;

				-- menu selects
				-- active palette select
				if rclick = '1' and
					newPointer.x > MENU_Active_Palette_X and
					newPointer.x < MENU_Palette_X and
					newPointer.y(6 downto 2) < 4
				then
					activePaletteHover <= newPointer.y(3 downto 2);
				end if;

				-- palette select
				if rclick = '1' and
					newPointer.x > MENU_Palette_X and
					newPointer.y(6 downto 2) < 16
				then
					paletteHover <= newPointer.y(5 downto 2);
					-- set the selected palette color to the activePalette, at activePaletteHover position
					activePalette(to_integer(activePaletteHover)) <= newPointer.y(5 downto 2);
				end if;

				segData <= unsigned(newPointerX(7 downto 0));

				if lclick = '1' then
					state := memAddr;
				end if;
			end if;

			-- memory state machine & pointer pos write
			case state is
				when memAddr =>
					we1 <= '0';
					addr1 <= getMemAddr(newPointer.y, newPointer.x);
					state := memWait;

				when memWait =>
					state := memUpdate;

				when memUpdate =>
					we1 <= '1';
					cdata := std_logic_vector(dataout1);
					demux(std_logic_vector(activePaletteHover), to_integer(newPointer.x(1 downto 0)), cdata);
					datain1 <= unsigned(cdata);
					state := memIdle;

				when others =>
					state := memIdle;

			end case;

			if rst = '1' then
				we1 <= '0';
				state := memIdle;
			end if;
		end if;
	end process;

end architecture;

