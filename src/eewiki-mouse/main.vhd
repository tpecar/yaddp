library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

architecture main_rtl of main is
	signal rst : std_logic;
  signal mousedata : std_logic_vector(23 downto 0);
  signal ps2ready : std_logic;
	signal segData : unsigned(7 downto 0);
begin
	mouse : entity work.ps2_mouse(logic)
		port map(clk, rst, n2_dsn_inout.PS2C, n2_dsn_inout.PS2D, mousedata, ps2ready);
	seg7 : ENTITY WORK.Seg7(Behavioral) PORT MAP(clk, rst, n2_dsn_out.seg, n2_dsn_out.dp, n2_dsn_out.an, segData);

  rst <= not n2_dsn_in.btn(0);

	segData <= unsigned(mousedata(15 downto 8));

	process(clk) is
	begin
		if clk'event and clk = '1' then
      n2_dsn_out.Led <= (others => '0');
      n2_dsn_out.Led(1) <= n2_dsn_inout.PS2C;
      n2_dsn_out.Led(2) <= n2_dsn_inout.PS2D;
			if ps2ready = '1' then
				n2_dsn_out.Led(0) <= '1';
			end if;
		end if;
	end process;

end architecture;