----------------------------------------------------------------------------------------------------
-- Structures that provide access to Nexys 2 I/O pins.
----------------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

package n2 is
  type n2_in is record
    sw          :   std_logic_vector(7 downto 0);
    btn         :   std_logic_vector(3 downto 0);
    RsRx        :   std_logic;
  end record;

  type n2_out is record
    seg         :   std_logic_vector(0 to 6);
    dp          :   std_logic;
    an          :   std_logic_vector(3 downto 0);
    Led         :   std_logic_vector(7 downto 0);
    vgaRed      :   std_logic_vector(3 downto 1);
    vgaGreen    :   std_logic_vector(3 downto 1);
    vgaBlue     :   std_logic_vector(3 downto 2);
    Hsync       :   std_logic;
    Vsync       :   std_logic;
    RsTx        :   std_logic;
  end record;

  type n2_inout is record
    PS2D        :   std_logic;
    PS2C        :   std_logic;
  end record;

  -- default states for in (as reported to design)
  constant n2_in_default : n2_in := (
    sw        => (others => '0'),
    btn       => (others => '0'),
    RsRx      => '0'
  );

  -- default states for out (as reported to toplevel)
  constant n2_out_default : n2_out := (
    seg       => (others => '0'),
    dp        => '0',
    an        => (others => '1'),
    Led       => (others => '0'),
    vgaRed    => (others => '0'),
    vgaGreen  => (others => '0'),
    vgaBlue   => (others => '0'),
    Hsync     => '0',
    Vsync     => '0',
    RsTx      => '0'
  );

  -- default states for inout
  constant n2_inout_default : n2_inout := (
    PS2C      => 'Z',
    PS2D      => 'Z'
  );

  -- Nexys 2 main clock frequency
  constant CLK_FREQ : integer := 50 * (10**6);

end package;

----------------------------------------------------------------------------------------------------
-- Entity that interfaces user design to toplevel module
-- User design is implemented in main.main_rtl architecture in main.vhd (user supplied)
----------------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.n2.all;
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

entity main is
  port (
    -- clk has to be a separate signal due to possible routing issues
    clk         : in  std_logic;

    -- non-driven signals will get default state as specified by n2_in/out/inout_default
    n2_dsn_in     : in    n2_in     := n2_in_default;
    n2_dsn_out    : out   n2_out    := n2_out_default;
    n2_dsn_inout  : inout n2_inout  := n2_inout_default
  );
begin
end entity;