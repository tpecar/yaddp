----------------------------------------------------------------------------------------------------
-- common combinatorics
----------------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.rangelib.all;

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

package comb is
  --------------------------------------------------------------------------------------------------
  -- counter
  procedure counter(vio_val : inout unsigned; constant c_max : integer);

  --------------------------------------------------------------------------------------------------
  -- prescaler
  procedure prescaler(
    vio_val   : inout unsigned; vo_enable : out std_logic;
    constant c_div : integer
  );

  --------------------------------------------------------------------------------------------------
  -- decoder
  function decoder(v_in : unsigned)
    return std_logic_vector;

  --------------------------------------------------------------------------------------------------
  -- encoder
  function encoder(v_in : std_logic_vector)
    return unsigned;

  --------------------------------------------------------------------------------------------------
  -- mux
  -- the selector is of integer type - if used in logic, the procedure can be called as
  --    mux(vi_in, to_integer(unsigned(vi_select)), vo_out);
  procedure mux(
    vi_in : in std_logic_vector;
    vi_select : in integer;
    vo_out : out std_logic_vector
  );

  --------------------------------------------------------------------------------------------------
  -- demux
  procedure demux(
    vi_in : in std_logic_vector;
    vi_select : in integer;
    vo_out : inout std_logic_vector
  );
  
end package;

----------------------------------------------------------------------------------------------------

package body comb is
  --------------------------------------------------------------------------------------------------
  -- counter
  procedure counter(vio_val : inout unsigned; constant c_max : integer)
  is
  begin
    if vio_val = to_unsigned(c_max, vio_val'length) then
      vio_val := to_unsigned(0, vio_val'length);
    else
      vio_val := vio_val + 1;
    end if;
  end procedure;

  --------------------------------------------------------------------------------------------------
  -- prescaler
  procedure prescaler(
    vio_val   : inout unsigned; vo_enable : out std_logic;
    constant c_div : integer
  )
  is
  begin
    if vio_val = to_unsigned(c_div - 1, vio_val'length) then
      vio_val := to_unsigned(0, vio_val'length);
      vo_enable := '1';
    else
      vio_val := vio_val + 1;
      vo_enable := '0';
    end if;
  end procedure;

  --------------------------------------------------------------------------------------------------
  -- decoder
  function decoder(v_in : unsigned)
    return std_logic_vector is
    variable v_out : std_logic_vector(2**v_in'length-1 downto 0);
  begin
    for v_in_value in 0 to 2**v_in'length-1 loop
      v_out(v_in_value) :=
        to_std_logic(to_unsigned(v_in_value, v_in'length) = v_in);
    end loop;
    return v_out;
  end function;

  --------------------------------------------------------------------------------------------------
  -- encoder
  function encoder(v_in : std_logic_vector)
    return unsigned is
    variable v_out : unsigned(to_high(v_in'length) downto 0) := (others => '0');
  begin
    -- lsb priority encoder
    for v_out_value in 0 to v_in'length loop
      if v_in(v_out_value) = '1' then
        v_out := to_unsigned(v_out_value, to_length(v_in'length));
        exit;
      end if;
    end loop;
    return v_out;
  end function;

  --------------------------------------------------------------------------------------------------
  -- mux
  --
  -- Be aware that out of range values mean that the vo_out retains its state (unmodified).
  --
  procedure mux(
    vi_in : in std_logic_vector;
    vi_select : in integer;
    vo_out : out std_logic_vector
  ) is
  begin
    -- remap vi_in to subvectors
    for v_cbit in 0 to vi_in'high loop
      if v_cbit / vo_out'length = vi_select then
        vo_out(v_cbit mod vo_out'length) := vi_in(v_cbit);
      end if;
    end loop;
  end;

  --------------------------------------------------------------------------------------------------
  -- demux
  --
  -- Be aware that this demux will only change the subsection of the output vector that got
  -- selected, while retaining the state of other bits in the vector. This could cause the output
  -- vector to be synthesized as registered.
  --
  -- To avoid that, set whole output vector to some constant before calling the procedure.
  --
  procedure demux(
    vi_in : in std_logic_vector;
    vi_select : in integer;
    vo_out : inout std_logic_vector
  ) is
  begin
    for v_cbit in 0 to vo_out'high loop
      if v_cbit / vi_in'length = vi_select then
        vo_out(v_cbit) := vi_in(v_cbit mod vi_in'length);
      end if;
    end loop;
  end;
end package body;
