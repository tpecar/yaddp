----------------------------------------------------------------------------------------------------
-- Logic vector to numerical output conversion.
----------------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.comb.all;

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

package numo is
  --------------------------------------------------------------------------------------------------
  -- convert 4-bit binary to one 7-segment display digit (cathode driver)

  -- constrained array types can only be returned as subtypes
  subtype hex2seg7ct_T is std_logic_vector(6 downto 0);

  function hex2seg7ct(v_in : unsigned(3 downto 0))
    return hex2seg7ct_T;

  --------------------------------------------------------------------------------------------------
  -- binary to BCD
  -- vo_out can be smaller than required, higher numerals will be discarded
  procedure binvec2bcd(vi_in : in unsigned; vo_out : out std_logic_vector);

end package;

----------------------------------------------------------------------------------------------------

package body numo is
  --------------------------------------------------------------------------------------------------
  -- convert 4-bit binary to one 7-segment display digit (cathode driver)
  function hex2seg7ct(v_in : unsigned(3 downto 0))
    return hex2seg7ct_T is

    type seg7ct_T is array(0 to 15) of std_logic_vector(6 downto 0);

    -- Do note that on Nexys2/4 the cathode pins are inverted (due to the design of the board).
    -- Also, since we are using a (6 downto 0) notation here, the first (CA) segment value is
    -- represented as the rightmost bit.
    constant seg7ct : seg7ct_T := (
    -- 0          1          2          3
      "1000000", "1111001", "0100100", "0110000",
    -- 4          5          6          7
      "0011001", "0010010", "0000010", "1111000",
    -- 8          9          A          b
      "0000000", "0010000", "0001000", "0000011",
    -- C          d          E          F
      "1000110", "0100001", "0000110", "0001110"
    );
  begin
    return seg7ct(to_integer(v_in));
  end function;

  procedure binvec2bcd(vi_in : in unsigned; vo_out : out std_logic_vector)
  is
    constant max_digits : integer := vo_out'length / 4;

    variable carry : integer := 0;
    variable sum   : integer;
    variable cur_digit : std_logic_vector(3 downto 0);
  begin
    vo_out := (others => '0');

    -- DO NOTE: check the loop ranges very well, since this can upset the synthesis.
    --
    -- Originally the range went from 0 to max_digits (one iteration too much), which caused the
    -- cur_digit_idx to be invalid, and any variables/signals that used it/based their calculation
    -- on it, were silently __ignored__
    --
    -- However, the loop variable could still be tested for its value, the comparison is also broken
    -- (some values in the range are skipped)
    --
    -- The invalid index also propagated into procedure calls, event through a
    -- variable - we've probably found a bug and entered a world of pain here.

    for cur_digit_idx in 0 to max_digits-1 loop
      cur_digit := (others => '0');

      -- we assume that vo_out length is a multiple of 4
      mux(std_logic_vector(vi_in), cur_digit_idx, cur_digit);

      sum := to_integer(unsigned(cur_digit)) + carry;
      carry := sum / 10;
      cur_digit := std_logic_vector(to_unsigned(sum mod 10, cur_digit'length));

      demux(cur_digit, cur_digit_idx, vo_out);
    end loop;
  end procedure;
end package body;

----------------------------------------------------------------------------------------------------
-- convert binary vector to 7-segment digits, with refresh rate
--
-- As a general rule, all logic that has to maintain an internal state should reside in its own
-- entity.
--
-- While in theory it would be better to implement it as a procedure with a state record, the
-- 1993 VHDL does not allow for unconstrained elements in records and VHDL 2008 support is ...
-- limited, to say in the most polite of ways.
--
-- An interesting workaround would be to use pointers (access types) to unconstrained elements,
-- which both the Vivado simulator and the ISE synthesis tool seem to compile without issue, but
-- this clashes with the structured design method since the assignment of state structures would
-- just copy the reference of the access type and that would cause the next state logic to dirty
-- the current state. We would have to be aware of pointers - initialize them to correct sizes
-- in arch declaration section, manually copy the states using new(ptr), etc.
--
-- It would get messy, not to mention that it would break somewhere due to tooling issues, so we
-- will rather stick to the olden ways.
-- Hopefully we will get proper 2008 support in the next millenia.
----------------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.n2.all;
use work.rangelib.all;
use work.comb.all;
use work.numo.all;
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

entity hexvec2seg7 is
  generic(
    cfg_refresh_Hz : integer
  );
  port(
    i_hex : in unsigned;
    o_ct : out std_logic_vector;
    o_an : out std_logic_vector;

    clk : std_logic;
    rst : std_logic
  );
begin

end entity;

architecture hexvec2seg7_rtl of hexvec2seg7 is
  constant num_digits : integer := o_an'length;
  constant digit_ticks : integer := CLK_FREQ / (num_digits * cfg_refresh_Hz);
  
  type i_T is record
    hex : unsigned(i_hex'range);
  end record;
  signal i : i_T;

  type o_T is record
    ct : std_logic_vector(o_ct'range);
    an : std_logic_vector(o_an'range);
  end record;
  constant o_reset : o_T := (
    ct => (others => '1'),
    an => (others => '1')
  );
  signal o : o_T;

  type reg is record
    cur_digit_idx : unsigned(to_high(num_digits) downto 0);
    prescaler : unsigned(to_high(digit_ticks) downto 0);
  end record;
  constant reg_reset : reg := (
    cur_digit_idx => (others => '0'),
    prescaler => (others => '0')
  );

  signal r, rin : reg;

begin
  i.hex <= i_hex;

  -- output is directly driven
  o_ct <= o.ct;
  o_an <= o.an;


  comb: process(rst, i, r) is
    -- auxiliary signals, required for calculation, but not part of the state
    variable next_digit : std_logic;
    variable cur_digit : std_logic_vector(3 downto 0);
    -- new state
    variable vr : reg;
  begin
    vr := r;

    cur_digit := (others => '0');
    mux(std_logic_vector(i.hex), to_integer(vr.cur_digit_idx), cur_digit);
    o.ct <= hex2seg7ct(unsigned(cur_digit));

    -- anode driven inverted
    o.an <= not decoder(vr.cur_digit_idx);

    prescaler(vr.prescaler, next_digit, digit_ticks);

    if next_digit = '1' then
      vr.cur_digit_idx := vr.cur_digit_idx + 1;
      if to_integer(vr.cur_digit_idx) = num_digits then
        vr.cur_digit_idx := (others => '0');
      end if;
    end if;

    if rst = '1' then
      vr := reg_reset;
      o <= o_reset;
    end if;

    rin <= vr;
  end process;

  seq: process(clk) is
  begin
    if rising_edge(clk) then
      r <= rin;
    end if;
  end process;
end architecture;