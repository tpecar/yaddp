----------------------------------------------------------------------------------------------------
-- string manipulation --
----------------------------------------------------------------------------------------------------
-- Partially based on the grlib-gpl-2017.3-b4208/lib/grlib/stdlib/stdlib.vhd,
-- which is distributed as a part of GRLIB VHDL IP LIBRARY by Gaisler Research under GPL license
----------------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_TEXTIO.ALL;

library STD;
use STD.TEXTIO.ALL;

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- Do note that when calling a function, its return value has to be explicitly caught by a variable,
-- otherwise the compiler confusingly complains about expecting a 'void' type in place of the
-- last argument
--
package strlib is

  -- dummy void type and shared variable to catch an output of an overloaded operator function
  type T_None is (none);
  shared variable stdout : T_None;

  --------------------------------------------------------------------------------------------------
  -- abbreviation, used for type qualifiers
  -- VHDL differentiates between type casting (made explicitly through a function call - type(x))
  -- and type indication (made through a type qualifier - type'(x))
  subtype s is string;

  --------------------------------------------------------------------------------------------------
  -- string + string
  function "*" (v_prefix : string; v_value : string)
    return string;

  --------------------------------------------------------------------------------------------------
  -- string + vector(binary)
  function "*" (v_prefix : string; v_value : std_logic_vector)
    return string;
  
  -- start of chain
  function "*" (v_value : std_logic_vector; v_postfix : string)
    return string;

  --------------------------------------------------------------------------------------------------
  type T_Hex is (HEX);
  type T_Hex_Prefix is record
    prefix : line;
  end record;

  -- string + vector(hex)
  -- mid-chain
  function "*" (v_prefix : string; v_keyword : T_Hex)
    return T_Hex_Prefix;
  function "*" (v_prefix : T_Hex_Prefix; v_value : std_logic_vector)
    return string;
  
  -- start of chain
  function "*" (v_keyword : T_Hex; v_value : std_logic_vector)
    return string;

  --------------------------------------------------------------------------------------------------
  -- standard type attributes to string

  type T_GetAttr is (GETATTR);
  type T_GetAttr_Prefix is record
    -- using pointers to data structures since they can be unconstrained during elaboration
    prefix : line;
  end record;

  function "*" (v_prefix : string; v_keyword : T_GetAttr)
    return T_GetAttr_Prefix;
  
  -- std_logic_vector attributes
  function "*" (v_prefix : T_GetAttr_Prefix; v_value : std_logic_vector)
    return string;
  -- unsigned attributes
  function "*" (v_prefix : T_GetAttr_Prefix; v_value : unsigned)
    return string;
  
  -- first in chain
  function "*" (v_keyword : T_GetAttr; v_value : std_logic_vector)
    return string;
  function "*" (v_keyword : T_GetAttr; v_value : unsigned)
    return string;
  
  --------------------------------------------------------------------------------------------------
  -- string + integer
  function "*" (v_prefix : string; v_value : integer)
    return string;
  
  -- start of chain
  function "*" (v_value : integer; v_postfix : string)
    return string;

  --------------------------------------------------------------------------------------------------
  -- string + real
  function "*" (v_prefix : string; v_value : real)
    return string;

  -- start of chain
  function "*" (v_value : real; v_postfix : string)
    return string;
  
  --------------------------------------------------------------------------------------------------
  -- string length trimming/alignment

  type T_LTrim is (LTRIM);
  type T_LTrim_Prefix is record
    prefix : line;
  end record;

  function "*" (v_prefix : string; v_keyword : T_LTrim)
    return T_LTrim_Prefix;
  function "*" (v_prefix : T_LTrim_Prefix; v_len : integer)
    return string;

  type T_RTrim is (RTRIM);
  type T_RTrim_Prefix is record
    prefix : line;
  end record;

  function "*" (v_prefix : string; v_keyword : T_RTrim)
    return T_RTrim_Prefix;
  function "*" (v_prefix : T_RTrim_Prefix; v_len : integer)
    return string;

  --------------------------------------------------------------------------------------------------
  -- adds newline, returns string
  type T_Endl is (endl);
  impure function "*" (v_result : string; v_endl : T_Endl)
    return string;

  --------------------------------------------------------------------------------------------------
  -- adds newline, prints to stdout
  impure function "*" (v_result : string; v_endl : T_Endl)
    return T_None;

end package;

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

package body strlib is

  --------------------------------------------------------------------------------------------------
  -- single digit integer to string conversion, from stdlib.vhd
  -- internal function
  function todec(i:integer) return character is
  begin
    case i is
    when 0 => return('0');
    when 1 => return('1');
    when 2 => return('2');
    when 3 => return('3');
    when 4 => return('4');
    when 5 => return('5');
    when 6 => return('6');
    when 7 => return('7');
    when 8 => return('8');
    when 9 => return('9');
    when others => return('0');
    end case;
  end;

  --------------------------------------------------------------------------------------------------
  -- integer to string conversion, from stdlib.vhd
  -- internal function
  function tost(i : integer) return string is
    variable L : line;
    variable s, x : string(1 to 128);
    variable n, tmp : integer := 0;
  begin
    tmp := i;
    if i < 0 then tmp := -i; end if;
    loop
      s(128-n) := todec(tmp mod 10);
      tmp := tmp / 10;
      n := n+1;
      if tmp = 0 then exit; end if;
    end loop;
    x(1 to n) := s(129-n to 128);
    if i < 0 then return "-" & x(1 to n); end if;
    return(x(1 to n));
  end;

  --------------------------------------------------------------------------------------------------
  -- real to string conversion, from stdlib.vhd
  -- internal function
  function tost(r: real) return string is
    variable x: real;
    variable i,j: integer;
    variable s: string(1 to 30);
    variable c: character;
  begin
    if r = 0.0 then
      return "0.0000";
    elsif r < 0.0 then
      return "-" & tost(-r);
    elsif r < 0.001 then
      x:=r; i:=0;
      while x<1.0 loop x:=x*10.0; i:=i+1; end loop;
      return tost(x) & "e-" & tost(i);
    elsif r >= 1000000.0 then
      x:=10000000.0; i:=6;
      while r>=x loop x:=x*10.0; i:=i+1; end loop;
      return tost(10.0*r/x) & "e+" & tost(i);
    else
      i:=0; x:=r+0.00005;
      while x >= 10.0 loop x:=x/10.0; i:=i+1; end loop;
      j := 1;
      while i > -5 loop
        if x >= 9.0 then c:='9'; x:=x-9.0;
        elsif x >= 8.0 then c:='8'; x:=x-8.0;
        elsif x >= 7.0 then c:='7'; x:=x-7.0;
        elsif x >= 6.0 then c:='6'; x:=x-6.0;
        elsif x >= 5.0 then c:='5'; x:=x-5.0;
        elsif x >= 4.0 then c:='4'; x:=x-4.0;
        elsif x >= 3.0 then c:='3'; x:=x-3.0;
        elsif x >= 2.0 then c:='2'; x:=x-2.0;
        elsif x >= 1.0 then c:='1'; x:=x-1.0;
        else c:='0';
        end if;
        s(j) := c;
        j:=j+1;
        if i=0 then s(j):='.'; j:=j+1; end if;
        i:=i-1;
        x := x * 10.0;
      end loop;
      return s(1 to j-1);
    end if;    
  end tost;

  --------------------------------------------------------------------------------------------------
  function "*" (v_prefix : string; v_value : string)
    return string is
  begin
    return v_prefix & v_value;
  end function;

  --------------------------------------------------------------------------------------------------
  function "*" (v_prefix : string; v_value : std_logic_vector)
    return string is
    variable v_out : line;
  begin
    v_out := new string'(v_prefix);
    write(v_out, to_bitvector(v_value));
    return v_out.all;
  end function;

  function "*" (v_value : std_logic_vector; v_postfix : string)
    return string is
  begin
    return "" * v_value * v_postfix;
  end function;

  --------------------------------------------------------------------------------------------------
  function "*" (v_prefix : string; v_keyword : T_Hex)
    return T_Hex_Prefix is
    variable v_Hex_Prefix : T_Hex_Prefix;
  begin
    v_Hex_Prefix.prefix := new string'(v_prefix);
    return v_Hex_Prefix;
  end function;

  function "*" (v_prefix : T_Hex_Prefix; v_value : std_logic_vector)
    return string is
    variable v_out : line;
  begin
    -- you cannot directly provide a record element as an argument,
    -- so we have to copy the pointer to a standalone variable
    v_out := v_prefix.prefix;

    hwrite(v_out, v_value, RIGHT, 0);
    return v_out.all;
  end function;

  function "*" (v_keyword : T_Hex; v_value : std_logic_vector)
    return string is
  begin
    return "" * HEX * v_value;
  end function;

  --------------------------------------------------------------------------------------------------
  function "*" (v_prefix : string; v_keyword : T_GetAttr)
    return T_GetAttr_Prefix is
    variable v_GetAttr_Prefix : T_GetAttr_Prefix;
  begin
    v_GetAttr_Prefix.prefix := new string'(v_prefix);
    return v_GetAttr_Prefix;
  end function;
  
  -- std_logic_vector attributes
  function "*" (v_prefix : T_GetAttr_Prefix; v_value : std_logic_vector)
    return string is
  begin
    return  v_prefix.prefix.all *
            s'("std_logic_vector [ ") *
            s'("Hi ") * v_value'high * s'(" Lo ") * v_value'low * s'(" Len ") * v_value'length *
            s'(" ]");
  end function;

  -- unsigned attributes
  function "*" (v_prefix : T_GetAttr_Prefix; v_value : unsigned)
    return string is
  begin
    return  v_prefix.prefix.all *
            s'("unsigned [") *
            s'(" Hi ") * v_value'high * s'(" Lo ") * v_value'low * s'(" Len ") * v_value'length *
            s'(" Val ") * to_integer(v_value) *
            s'(" ]");
  end function;
  
  -- first in chain
  function "*" (v_keyword : T_GetAttr; v_value : std_logic_vector)
    return string is
  begin
    return "" * GETATTR * v_value;
  end function;

  function "*" (v_keyword : T_GetAttr; v_value : unsigned)
    return string is
  begin
    return "" * GETATTR * v_value;
  end function;

  --------------------------------------------------------------------------------------------------
  function "*" (v_prefix : string; v_value : integer)
    return string is
  begin
    return v_prefix & tost(v_value);
  end function;

  function "*" (v_value : integer; v_postfix : string)
    return string is
  begin
    return "" * v_value * v_postfix;
  end function;

  --------------------------------------------------------------------------------------------------
  function "*" (v_prefix : string; v_value : real)
    return string is
  begin
    return v_prefix & tost(v_value);
  end function;

  function "*" (v_value : real; v_postfix : string)
    return string is
  begin
    return "" * v_value * v_postfix;
  end function;

  --------------------------------------------------------------------------------------------------
  function "*" (v_prefix : string; v_keyword : T_LTrim)
    return T_LTrim_Prefix is
    variable v_LTrim_Prefix : T_LTrim_Prefix;
  begin
    v_LTrim_Prefix.prefix := new string'(v_prefix);
    return v_LTrim_Prefix;
  end function;
  function "*" (v_prefix : T_LTrim_Prefix; v_len : integer)
    return string is
    variable v_out : line := new string'("");
  begin
    write(v_out, v_prefix.prefix.all, left, v_len);
    return v_out.all;
  end function;

  function "*" (v_prefix : string; v_keyword : T_RTrim)
    return T_RTrim_Prefix is
    variable v_RTrim_Prefix : T_RTrim_Prefix;
  begin
    v_RTrim_Prefix.prefix := new string'(v_prefix);
    return v_RTrim_Prefix;
  end function;
  function "*" (v_prefix : T_RTrim_Prefix; v_len : integer)
    return string is
    variable v_out : line := new string'("");
  begin
     write(v_out, v_prefix.prefix.all, right, v_len);
    return v_out.all;
  end function;
  
  --------------------------------------------------------------------------------------------------
  -- adds newline, returns string
  impure function "*" (v_result : string; v_endl : T_Endl)
    return string is
    -- in VHDL we cannot explicitly get a reference to an object, so we have to instantiate
    -- a new one using the new operator
    variable v_resultPtr : line := new string'(v_result);
  begin
    -- in VHDL, character is an enumerated type, so we get a specific character value through
    -- enumeration index (which corresponds to ASCII code)
    write(v_resultPtr, character'val(10));
    return v_resultPtr.all;
  end function;

  --------------------------------------------------------------------------------------------------
  -- adds newline, prints to stdout
  impure function "*" (v_result : string; v_endl : T_Endl)
    return T_None is
    variable v_resultPtr : line;
  begin
    v_resultPtr := new string'(v_result);
    writeline(output, v_resultPtr);
    return none;
  end function;

end package body;
