----------------------------------------------------------------------------------
-- Designed by MP & TP.
--
-- An UNISIM mapped dual ported 64k bit (8k x 8 bit) RAM
-- for faster synthesis, fun & profit.
-- 
-- Create Date:    14:16:00 08/15/2018 
--
----------------------------------------------------------------------------------
-- The following instantiations are referenced from
-- spartan3_hdl__ISE_10.1_2008.pdf : Libraries Guide, 10.1

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

Library UNISIM;
use UNISIM.vcomponents.all;

entity ram64k is
    port (
        -- reset whole memory - requires 2048 clock cycles
        rst : in std_logic;
        clk : in std_logic;
        
        -- 0 when in reset mode
        ready : out std_logic := '1';

        -- 1. port
        addr0       : in unsigned(12 downto 0)  := (others => '0');
        we0         : in std_logic              := '0';
        datain0     : in unsigned(7 downto 0)   := (others => '0');
        dataout0    : out unsigned(7 downto 0)  := (others => '0');

        -- 2. port
        addr1       : in unsigned(12 downto 0)  := (others => '0');
        we1         : in std_logic              := '0';
        datain1     : in unsigned(7 downto 0)   := (others => '0');
        dataout1    : out unsigned(7 downto 0)  := (others => '0')
    );
begin
end entity;

architecture Behavioral of ram64k is
    -- Using the RAMB16_S9_S9 unit --

    -- RAMB16_S9_S9: Virtex-II/II-Pro, Spartan-3/3E 2k x 8 + 1 Parity bit Dual-Port RAM
    -- Xilinx HDL Libraries Guide, version 10.1.2

    -- BANK 0 --
    signal b0_DOA   : std_logic_vector(7 downto 0);
    signal b0_DOB   : std_logic_vector(7 downto 0);

    signal b0_ADDRA : std_logic_vector(10 downto 0);
    signal b0_ADDRB : std_logic_vector(10 downto 0);

    signal b0_DIA   : std_logic_vector(7 downto 0);
    signal b0_DIB   : std_logic_vector(7 downto 0);

    signal b0_ENA   : std_logic;
    signal b0_ENB   : std_logic;
    signal b0_WEA   : std_logic;
    signal b0_WEB   : std_logic;

    -- BANK 1 --
    signal b1_DOA   : std_logic_vector(7 downto 0);
    signal b1_DOB   : std_logic_vector(7 downto 0);

    signal b1_ADDRA : std_logic_vector(10 downto 0);
    signal b1_ADDRB : std_logic_vector(10 downto 0);

    signal b1_DIA   : std_logic_vector(7 downto 0);
    signal b1_DIB   : std_logic_vector(7 downto 0);

    signal b1_ENA   : std_logic;
    signal b1_ENB   : std_logic;
    signal b1_WEA   : std_logic;
    signal b1_WEB   : std_logic;

    -- BANK 2 --
    signal b2_DOA   : std_logic_vector(7 downto 0);
    signal b2_DOB   : std_logic_vector(7 downto 0);

    signal b2_ADDRA : std_logic_vector(10 downto 0);
    signal b2_ADDRB : std_logic_vector(10 downto 0);

    signal b2_DIA   : std_logic_vector(7 downto 0);
    signal b2_DIB   : std_logic_vector(7 downto 0);

    signal b2_ENA   : std_logic;
    signal b2_ENB   : std_logic;
    signal b2_WEA   : std_logic;
    signal b2_WEB   : std_logic;

    -- BANK 3 --
    signal b3_DOA   : std_logic_vector(7 downto 0);
    signal b3_DOB   : std_logic_vector(7 downto 0);

    signal b3_ADDRA : std_logic_vector(10 downto 0);
    signal b3_ADDRB : std_logic_vector(10 downto 0);

    signal b3_DIA   : std_logic_vector(7 downto 0);
    signal b3_DIB   : std_logic_vector(7 downto 0);

    signal b3_ENA   : std_logic;
    signal b3_ENB   : std_logic;
    signal b3_WEA   : std_logic;
    signal b3_WEB   : std_logic;

    -- reset whole memory - requires 2048 clock cycles
    signal inReset : std_logic := '0';
    signal caddr : unsigned(10 downto 0) := (others => '0');

begin
    -- BANK 0 --
    RAMB16_S9_S9_bank0_inst : RAMB16_S9_S9
    generic map (
        INIT_A => X"000",   -- Value of output RAM registers on Port A at startup
        INIT_B => X"000",   -- Value of output RAM registers on Port B at startup
        SRVAL_A => X"000",  -- Port A ouput value upon SSR assertion
        SRVAL_B => X"000",  -- Port B ouput value upon SSR assertion
        WRITE_MODE_A => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
        WRITE_MODE_B => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
        SIM_COLLISION_CHECK => "ALL"   -- "NONE", "WARNING", "GENERATE_X_ONLY", "ALL"
        -- we will skip the initial content specification
    )
    port map (
        DOA     => b0_DOA,      -- Port A 8-bit Data Output
        DOB     => b0_DOB,      -- Port B 8-bit Data Output
        DOPA    => open,        -- Port A 1-bit Parity Output
        DOPB    => open,        -- Port B 1-bit Parity Output

        ADDRA   => b0_ADDRA,    -- Port A 11-bit Address Input
        ADDRB   => b0_ADDRB,    -- Port B 11-bit Address Input

        CLKA    => clk,         -- Port A Clock
        CLKB    => clk,         -- Port B Clock

        DIA     => b0_DIA,      -- Port A 8-bit Data Input
        DIB     => b0_DIB,      -- Port B 8-bit Data Input
        DIPA    => "0",         -- Port A 1-bit parity Input
        DIPB    => "0",         -- Port B 1-bit parity Input
        ENA     => b0_ENA,      -- Port A RAM Enable Input
        ENB     => b0_ENB,      -- Port B RAM Enable Input
        SSRA    => '0',         -- Port A Synchronous Set/Reset Input
        SSRB    => '0',         -- Port B Synchronous Set/Reset Input
        WEA     => b0_WEA,      -- Port A Write Enable Input
        WEB     => b0_WEB       -- Port B Write Enable Input
    );

    -- BANK 1 --
    RAMB16_S9_S9_bank1_inst : RAMB16_S9_S9
    generic map (
        INIT_A => X"000",   -- Value of output RAM registers on Port A at startup
        INIT_B => X"000",   -- Value of output RAM registers on Port B at startup
        SRVAL_A => X"000",  -- Port A ouput value upon SSR assertion
        SRVAL_B => X"000",  -- Port B ouput value upon SSR assertion
        WRITE_MODE_A => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
        WRITE_MODE_B => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
        SIM_COLLISION_CHECK => "ALL"   -- "NONE", "WARNING", "GENERATE_X_ONLY", "ALL"
        -- we will skip the initial content specification
    )
    port map (
        DOA     => b1_DOA,      -- Port A 8-bit Data Output
        DOB     => b1_DOB,      -- Port B 8-bit Data Output
        DOPA    => open,        -- Port A 1-bit Parity Output
        DOPB    => open,        -- Port B 1-bit Parity Output

        ADDRA   => b1_ADDRA,    -- Port A 11-bit Address Input
        ADDRB   => b1_ADDRB,    -- Port B 11-bit Address Input

        CLKA    => clk,         -- Port A Clock
        CLKB    => clk,         -- Port B Clock

        DIA     => b1_DIA,      -- Port A 8-bit Data Input
        DIB     => b1_DIB,      -- Port B 8-bit Data Input
        DIPA    => "0",         -- Port A 1-bit parity Input
        DIPB    => "0",         -- Port B 1-bit parity Input
        ENA     => b1_ENA,      -- Port A RAM Enable Input
        ENB     => b1_ENB,      -- Port B RAM Enable Input
        SSRA    => '0',         -- Port A Synchronous Set/Reset Input
        SSRB    => '0',         -- Port B Synchronous Set/Reset Input
        WEA     => b1_WEA,      -- Port A Write Enable Input
        WEB     => b1_WEB       -- Port B Write Enable Input
    );

    -- BANK 2 --
    RAMB16_S9_S9_bank2_inst : RAMB16_S9_S9
    generic map (
        INIT_A => X"000",   -- Value of output RAM registers on Port A at startup
        INIT_B => X"000",   -- Value of output RAM registers on Port B at startup
        SRVAL_A => X"000",  -- Port A ouput value upon SSR assertion
        SRVAL_B => X"000",  -- Port B ouput value upon SSR assertion
        WRITE_MODE_A => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
        WRITE_MODE_B => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
        SIM_COLLISION_CHECK => "ALL"   -- "NONE", "WARNING", "GENERATE_X_ONLY", "ALL"
        -- we will skip the initial content specification
    )
    port map (
        DOA     => b2_DOA,      -- Port A 8-bit Data Output
        DOB     => b2_DOB,      -- Port B 8-bit Data Output
        DOPA    => open,        -- Port A 1-bit Parity Output
        DOPB    => open,        -- Port B 1-bit Parity Output

        ADDRA   => b2_ADDRA,    -- Port A 11-bit Address Input
        ADDRB   => b2_ADDRB,    -- Port B 11-bit Address Input

        CLKA    => clk,         -- Port A Clock
        CLKB    => clk,         -- Port B Clock

        DIA     => b2_DIA,      -- Port A 8-bit Data Input
        DIB     => b2_DIB,      -- Port B 8-bit Data Input
        DIPA    => "0",         -- Port A 1-bit parity Input
        DIPB    => "0",         -- Port B 1-bit parity Input
        ENA     => b2_ENA,      -- Port A RAM Enable Input
        ENB     => b2_ENB,      -- Port B RAM Enable Input
        SSRA    => '0',         -- Port A Synchronous Set/Reset Input
        SSRB    => '0',         -- Port B Synchronous Set/Reset Input
        WEA     => b2_WEA,      -- Port A Write Enable Input
        WEB     => b2_WEB       -- Port B Write Enable Input
    );

    -- BANK 3 --
    RAMB16_S9_S9_bank3_inst : RAMB16_S9_S9
    generic map (
        INIT_A => X"000",   -- Value of output RAM registers on Port A at startup
        INIT_B => X"000",   -- Value of output RAM registers on Port B at startup
        SRVAL_A => X"000",  -- Port A ouput value upon SSR assertion
        SRVAL_B => X"000",  -- Port B ouput value upon SSR assertion
        WRITE_MODE_A => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
        WRITE_MODE_B => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
        SIM_COLLISION_CHECK => "ALL"   -- "NONE", "WARNING", "GENERATE_X_ONLY", "ALL"
        -- we will skip the initial content specification
    )
    port map (
        DOA     => b3_DOA,      -- Port A 8-bit Data Output
        DOB     => b3_DOB,      -- Port B 8-bit Data Output
        DOPA    => open,        -- Port A 1-bit Parity Output
        DOPB    => open,        -- Port B 1-bit Parity Output

        ADDRA   => b3_ADDRA,    -- Port A 11-bit Address Input
        ADDRB   => b3_ADDRB,    -- Port B 11-bit Address Input

        CLKA    => clk,         -- Port A Clock
        CLKB    => clk,         -- Port B Clock

        DIA     => b3_DIA,      -- Port A 8-bit Data Input
        DIB     => b3_DIB,      -- Port B 8-bit Data Input
        DIPA    => "0",         -- Port A 1-bit parity Input
        DIPB    => "0",         -- Port B 1-bit parity Input
        ENA     => b3_ENA,      -- Port A RAM Enable Input
        ENB     => b3_ENB,      -- Port B RAM Enable Input
        SSRA    => '0',         -- Port A Synchronous Set/Reset Input
        SSRB    => '0',         -- Port B Synchronous Set/Reset Input
        WEA     => b3_WEA,      -- Port A Write Enable Input
        WEB     => b3_WEB       -- Port B Write Enable Input
    );

    -- reset state machine
    process(clk, rst) is
    begin
        if clk'event and clk = '1' then
            if rst = '1' then
                inReset <= '1';
                -- reset operation
                ready <= '0';
                -- caddr already 0, set to 1 for next cycle
                caddr <= to_unsigned(1, caddr'length);
            end if;

            if inReset = '1' then
                caddr <= caddr + 1;
                -- overflow - went through all addresses
                if caddr = 0 then
                    inReset <= '0';
                    ready <= '1';
                end if;
            end if;
        end if;
    end process;

    -- bank switching
    process(
        inReset, caddr,
        we0, we1,
        addr0, addr1,
        datain0, datain1,
        b0_DOA, b1_DOA, b2_DOA, b3_DOA,
        b0_DOB, b1_DOB, b2_DOB, b3_DOB
    ) is
    begin
        -- default (no-op) state
        b0_ENA <= '0'; b1_ENA <= '0'; b2_ENA <= '0'; b3_ENA <= '0';
        b0_WEA <= '0'; b1_WEA <= '0'; b2_WEA <= '0'; b3_WEA <= '0';

        b0_ADDRA <= (others => '0'); b1_ADDRA <= (others => '0');
        b2_ADDRA <= (others => '0'); b3_ADDRA <= (others => '0');
        b0_DIA <= (others => '0'); b1_DIA <= (others => '0');
        b2_DIA <= (others => '0'); b3_DIA <= (others => '0');

        b0_ENB <= '0'; b1_ENB <= '0'; b2_ENB <= '0'; b3_ENB <= '0';
        b0_WEB <= '0'; b1_WEB <= '0'; b2_WEB <= '0'; b3_WEB <= '0';

        b0_ADDRB <= (others => '0'); b1_ADDRB <= (others => '0');
        b2_ADDRB <= (others => '0'); b3_ADDRB <= (others => '0');
        b0_DIB <= (others => '0'); b1_DIB <= (others => '0');
        b2_DIB <= (others => '0'); b3_DIB <= (others => '0');

        if inReset = '0' then -- normal operation
            -- 1. port
            case addr0(12 downto 11) is
                when "00" =>
                    b0_ENA <= '1'; b1_ENA <= '0'; b2_ENA <= '0'; b3_ENA <= '0';
                    b0_WEA <= we0; b1_WEA <= '0'; b2_WEA <= '0'; b3_WEA <= '0';
                    b0_ADDRA <= std_logic_vector(addr0(10 downto 0));
                    b0_DIA <= std_logic_vector(datain0); dataout0 <= unsigned(b0_DOA);

                when "01" =>
                    b0_ENA <= '0'; b1_ENA <= '1'; b2_ENA <= '0'; b3_ENA <= '0';
                    b0_WEA <= '0'; b1_WEA <= we0; b2_WEA <= '0'; b3_WEA <= '0';
                    b1_ADDRA <= std_logic_vector(addr0(10 downto 0));
                    b1_DIA <= std_logic_vector(datain0); dataout0 <= unsigned(b1_DOA);

                when "10" =>
                    b0_ENA <= '0'; b1_ENA <= '0'; b2_ENA <= '1'; b3_ENA <= '0';
                    b0_WEA <= '0'; b1_WEA <= '0'; b2_WEA <= we0; b3_WEA <= '0';
                    b2_ADDRA <= std_logic_vector(addr0(10 downto 0));
                    b2_DIA <= std_logic_vector(datain0); dataout0 <= unsigned(b2_DOA);

                when "11" =>
                    b0_ENA <= '0'; b1_ENA <= '0'; b2_ENA <= '0'; b3_ENA <= '1';
                    b0_WEA <= '0'; b1_WEA <= '0'; b2_WEA <= '0'; b3_WEA <= we0;
                    b3_ADDRA <= std_logic_vector(addr0(10 downto 0));
                    b3_DIA <= std_logic_vector(datain0); dataout0 <= unsigned(b3_DOA);
                
                when others =>
            end case;

            -- 2. port
            case addr1(12 downto 11) is
                when "00" =>
                    b0_ENB <= '1'; b1_ENB <= '0'; b2_ENB <= '0'; b3_ENB <= '0';
                    b0_WEB <= we1; b1_WEB <= '0'; b2_WEB <= '0'; b3_WEB <= '0';
                    b0_ADDRB <= std_logic_vector(addr1(10 downto 0));
                    b0_DIB <= std_logic_vector(datain1); dataout1 <= unsigned(b0_DOB);

                when "01" =>
                    b0_ENB <= '0'; b1_ENB <= '1'; b2_ENB <= '0'; b3_ENB <= '0';
                    b0_WEB <= '0'; b1_WEB <= we1; b2_WEB <= '0'; b3_WEB <= '0';
                    b1_ADDRB <= std_logic_vector(addr1(10 downto 0));
                    b1_DIB <= std_logic_vector(datain1); dataout1 <= unsigned(b1_DOB);

                when "10" =>
                    b0_ENB <= '0'; b1_ENB <= '0'; b2_ENB <= '1'; b3_ENB <= '0';
                    b0_WEB <= '0'; b1_WEB <= '0'; b2_WEB <= we1; b3_WEB <= '0';
                    b2_ADDRB <= std_logic_vector(addr1(10 downto 0));
                    b2_DIB <= std_logic_vector(datain1); dataout1 <= unsigned(b2_DOB);

                when "11" =>
                    b0_ENB <= '0'; b1_ENB <= '0'; b2_ENB <= '0'; b3_ENB <= '1';
                    b0_WEB <= '0'; b1_WEB <= '0'; b2_WEB <= '0'; b3_WEB <= we1;
                    b3_ADDRB <= std_logic_vector(addr1(10 downto 0));
                    b3_DIB <= std_logic_vector(datain1); dataout1 <= unsigned(b3_DOB);

                when others =>
            end case;
        else
            -- clear all banks at the same time
            b0_ENA <= '1'; b1_ENA <= '1'; b2_ENA <= '1'; b3_ENA <= '1';
            b0_WEA <= '1'; b1_WEA <= '1'; b2_WEA <= '1'; b3_WEA <= '1';

            b0_ADDRA <= std_logic_vector(caddr); b1_ADDRA <= std_logic_vector(caddr);
            b2_ADDRA <= std_logic_vector(caddr); b3_ADDRA <= std_logic_vector(caddr);

            b0_DIA <= (others => '0'); b1_DIA <= (others => '0');
            b2_DIA <= (others => '0'); b3_DIA <= (others => '0');

            dataout0 <= (others => '0');
            dataout1 <= (others => '0');
        end if;
    end process;

end architecture;




