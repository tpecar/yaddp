----------------------------------------------------------------------------------------------------
-- integer range calculation, manipulation & comparison --
----------------------------------------------------------------------------------------------------
-- For easier manipulation of ranges, used as parameters in modules.
----------------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

package rangelib is
  --------------------------------------------------------------------------------------------------
  -- range record definition
  type integer_range is record
    high  : integer;
    low   : integer;
    length  : integer;
  end record;

  type natural_range is record
    high  : natural;
    low   : natural;
    length  : natural;
   end record;

  --------------------------------------------------------------------------------------------------
  -- invalid values, defined so that we can test which values have been explicitly set
  -- in record initialization functions

  constant integer_invalid : integer := integer'high;
  constant integer_range_invalid : integer_range := (others => integer_invalid);

  constant natural_invalid : natural := natural'high;
  constant natural_range_invalid : natural_range := (others => natural_invalid);

  --------------------------------------------------------------------------------------------------
  function to_length(value : natural)
    return natural;

  function to_high(value : natural)
    return natural;

  function to_std_logic(value : boolean)
    return std_logic;

  -------------------------------------------------------------------------------------------------- 
  function to_integer_range(high : integer; low : integer)
    return integer_range;
  
  function to_integer_range(length : integer)
    return integer_range;

  function "=" (l : integer_range; r : integer_range)
    return boolean;
  
  function "/=" (l : integer_range; r : integer_range)
    return boolean;

  -------------------------------------------------------------------------------------------------- 
  function to_natural_range(high : natural; low : natural)
    return natural_range;

  function to_natural_range(length : natural)
    return natural_range;

  function "=" (l : natural_range; r : natural_range)
    return boolean;

  function "/=" (l : natural_range; r : natural_range)
    return boolean;

end package;

----------------------------------------------------------------------------------------------------

package body rangelib is
  -------------------------------------------------------------------------------------------------- 
  function to_length(value : natural)
    return natural is
  begin
    return natural(ceil(log2(real(value))));
  end function;

  function to_high(value : natural)
    return natural is
  begin
    return to_length(value)-1;
  end function;

  function to_std_logic(value : boolean)
    return std_logic is
  begin
    -- assumes active-high logic
    if value then return '1'; else return '0'; end if;
  end function;

  -------------------------------------------------------------------------------------------------- 
  function to_integer_range(high : integer; low : integer)
    return integer_range is
    variable o : integer_range := (high => high, low => low, length => high-low+1);
  begin
    return o;
  end function;

  function to_integer_range(length : integer)
    return integer_range is
  begin
    return to_integer_range(length-1, 0);
  end function;

  function "=" (l : integer_range; r : integer_range)
    return boolean is
  begin
    return l.high = r.high and l.low = r.low and l.length = r.length;
  end function;

  function "/=" (l : integer_range; r : integer_range)
    return boolean is
  begin
    return not "=" (l, r);
  end function;

  -------------------------------------------------------------------------------------------------- 
  function to_natural_range(high : natural; low : natural)
    return natural_range is
    variable o : natural_range := (high => high, low => low, length => high-low+1);
  begin
    return o;
  end function;

  function to_natural_range(length : natural)
    return natural_range is
  begin
    return to_natural_range(length-1, 0);
  end function;

  function "=" (l : natural_range; r : natural_range)
    return boolean is
  begin
    return l.high = r.high and l.low = r.low and l.length = r.length;
  end function;

  function "/=" (l : natural_range; r : natural_range)
    return boolean is
  begin
    return not "=" (l, r);
  end function;
  
end package body;
