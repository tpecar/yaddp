----------------------------------------------------------------------------------------------------
-- Nexys 2 toplevel module.
----------------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.n2.all;

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- toplevel entity

entity top is
  port
  (
    clk         : in    std_logic;
    seg         : out   std_logic_vector(0 to 6);
    dp          : out   std_logic;
    an          : out   std_logic_vector(3 downto 0);
    Led         : out   std_logic_vector(7 downto 0);
    sw          : in    std_logic_vector(7 downto 0);
    btn         : in    std_logic_vector(3 downto 0);
    vgaRed      : out   std_logic_vector(3 downto 1);
    vgaGreen    : out   std_logic_vector(3 downto 1);
    vgaBlue     : out   std_logic_vector(3 downto 2);
    Hsync       : out   std_logic;
    Vsync       : out   std_logic;
    PS2C        : inout std_logic;
    PS2D        : inout std_logic;
    RsRx        : in    std_logic;
    RsTx        : out   std_logic
  );
begin
end entity;

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

architecture top_rtl of top is
  -- in/out signals that bind to toplevel entity ports
  signal n2_top_in  : n2_in;
  signal n2_top_out : n2_out;
  signal n2_top_inout : n2_inout;
begin
  -- bind entity in/out to records
  n2_top_in.sw      <= sw;
  n2_top_in.btn     <= btn;
  n2_top_in.RsRx    <= RsRx;

  seg               <= n2_top_out.seg;
  dp                <= n2_top_out.dp;
  an                <= n2_top_out.an;
  Led               <= n2_top_out.Led;
  vgaRed            <= n2_top_out.vgaRed;
  vgaGreen          <= n2_top_out.vgaGreen;
  vgaBlue           <= n2_top_out.vgaBlue;
  Hsync             <= n2_top_out.Hsync;
  Vsync             <= n2_top_out.Vsync;
  RsTx              <= n2_top_out.RsTx;

  PS2C <= n2_top_inout.PS2C;
  PS2D <= n2_top_inout.PS2D;

  -- instantiate user design and map it to design signals
  user_design : entity work.main(main_rtl)
    port map(
      clk => clk,
      n2_dsn_in => n2_top_in,
      n2_dsn_out => n2_top_out,
      n2_dsn_inout => n2_top_inout
    );
  
end architecture;